
package PawnBot;

import PawnBot.pircbot.IrcException;
import PawnBot.pircbot.NickAlreadyInUseException;
import it.gotoandplay.smartfoxserver.data.User;
import it.gotoandplay.smartfoxserver.events.InternalEventObject;
import it.gotoandplay.smartfoxserver.extensions.AbstractExtension;
import it.gotoandplay.smartfoxserver.extensions.ExtensionHelper;
import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;

public class PawnBotMain
  extends AbstractExtension
{
  public String nick = "PawnBot";
  public String pass = "k5mnw35evn7y4ntwf";
  public String server = "irc.heuxe.org";
  public String lobby = "#staff";
  public String logs = "#pawn-logs";
  public String nsIdent = "This nickname is registered and protected.";
  public long messageDelay = 50L;
  public String quitMsg = "PawnBot - Shutting down...";
  public String nickserv = "NickServ";
  public String chanserv = "ChanServ";
  public String operserv = "OperServ";
  public PawnBot bot;
  public boolean shutdown = false;
  public HashMap listenList = new HashMap();
  public ExtensionHelper _server = ExtensionHelper.instance();
  
  public void init()
  {
    this.bot = new PawnBot(this.nick, this);
    this.bot.setMessageDelay(this.messageDelay);
    
    this.listenList.put("Chat", new Integer(1));
    
    login();
  }
  
  public void destroy()
  {
    this.shutdown = true;
    this.bot.quitServer(this.quitMsg);
    this.bot.dispose();
  }
  
  public void handleRequest(String cmd, ActionscriptObject ao, User u, int fromRoom) {}
  
  public void handleRequest(String cmd, String[] params, User u, int fromRoom) {}
  
  public void handleInternalEvent(InternalEventObject ieo) {}
  
  public void login()
  {
    int error = connect();
    if (error == 0)
    {
      login();
    }
    else if (error == 1)
    {
      this.bot.sendMessage(this.nickserv, "ghost " + this.nick + " " + this.pass);
      this.bot.changeNick(this.nick);
    }
  }
  
  public int connect()
  {
    try
    {
      try
      {
        this.bot.connect(this.server);
      }
      catch (NickAlreadyInUseException ni)
      {
        return 1;
      }
      return -1;
    }
    catch (IrcException ir)
    {
      return 0;
    }
    catch (IOException io)
    {
      return 0;
    }
  }
  
  public void joinChannel(String chan)
  {
    this.bot.sendMessage(this.chanserv, "invite " + chan);
  }
  
  public void identify()
  {
    this.bot.sendMessage(this.nickserv, "identify " + this.pass);
    this.bot.sendMessage(this.chanserv, "OP ALL");
  }
  
  public void sendMessageToAllChannels(String msg)
  {
    this.bot.sendMessage(this.lobby, msg);
    this.bot.sendMessage(this.logs, msg);
  }
  
  public void checkChannels()
  {
    if (this.bot.getChannels().length != 2)
    {
      joinChannel(this.lobby);
      joinChannel(this.logs);
    }
  }
  
  public void parseLogs(String log)
  {
    String timestamp = DateFormat.getDateTimeInstance(3, 3).format(new Date());
    String msg = "";
    String[] fields = log.split("/");
    boolean send = true;
    msg = msg + "[" + timestamp + "] ";
    if (fields[0].equals("C"))
    {
      if (this.listenList.containsKey(fields[1]))
      {
        String text = "";
        for (int i = 3; i < fields.length; i++) {
          text = text + fields[i] + "/";
        }
        text = text.substring(0, text.length() - 1);
        msg = msg + "(" + fields[1] + ") " + fields[2] + ": " + text;
      }
    }
    else
    {
      if (fields[0].equals("P"))
      {
        String text = "";
        for (int i = 3; i < fields.length; i++) {
          text = text + fields[i] + "/";
        }
        text = text.substring(0, text.length() - 1);
        msg = msg + "\00303(" + fields[1] + " > " + fields[2] + "): " + text;
        this.bot.sendMessage(this.lobby, msg);
        return;
      }
      if (fields[0].equals("S"))
      {
        msg = msg + "\00308Server: ";
        if (fields.length == 2)
        {
          msg = msg + fields[1];
        }
        else if (fields[1].equals("LOGIN"))
        {
          send = false;
        }
        else if (fields[1].equals("ANNOUNCE"))
        {
          String text = "";
          for (int i = 3; i < fields.length; i++) {
            text = text + fields[i] + "/";
          }
          text = text.substring(0, text.length() - 1);
          msg = msg + "Announcement from " + fields[2] + ": " + text;
        }
        else
        {
          if (fields[1].equals("KICK"))
          {
            String text = "";
            for (int i = 4; i < fields.length; i++) {
              text = text + fields[i] + "/";
            }
            text = text.substring(0, text.length() - 1);
            msg = msg + fields[2] + " kicked " + fields[3] + " (" + text + ")";
            this.bot.sendMessage(this.lobby, msg);
            return;
          }
          if (fields[1].equals("BAN"))
          {
            String text = "";
            for (int i = 5; i < fields.length; i++) {
              text = text + fields[i] + "/";
            }
            text = text.substring(0, text.length() - 1);
            if (fields[2].equals("IP")) {
              msg = msg + fields[3] + " banned the IP " + fields[4] + " (" + text + ")";
            } else if (fields[2].equals("ACCOUNT")) {
              msg = msg + fields[3] + " banned the Account " + fields[4] + " (" + text + ")";
            } else if (fields[2].equals("USER")) {
              msg = msg + fields[3] + " banned the User " + fields[4] + " (" + text + ")";
            } else if (fields[2].equals("KICKBAN")) {
              msg = msg + fields[3] + " kick-banned the User " + fields[4] + " (" + text + ")";
            }
            this.bot.sendMessage(this.lobby, msg);
            return;
          }
          if (fields[1].equals("UNBAN"))
          {
            msg = msg + fields[2] + " unbanned " + fields[3];
            this.bot.sendMessage(this.lobby, msg);
            return;
          }
          if (fields[1].equals("PROMOTE"))
          {
            String rank = "";
            if (fields[2].equals("ADMIN")) {
              rank = "Administrator";
            } else if (fields[2].equals("MOD")) {
              rank = "Moderator";
            }
            msg = msg + fields[3] + " promoted " + fields[4] + " to " + rank;
            this.bot.sendMessage(this.lobby, msg);
            return;
          }
          if (fields[1].equals("DEMOTE"))
          {
            String rank = "";
            if (fields[2].equals("USER")) {
              rank = "User";
            } else if (fields[2].equals("MOD")) {
              rank = "Moderator";
            }
            msg = msg + fields[3] + " demoted " + fields[4] + " to " + rank;
          }
          else if (fields[1].equals("DELETE"))
          {
            msg = msg + fields[2] + " deleted " + fields[3] + "'s account";
          }
          else if (fields[1].equals("CLOSEROOM"))
          {
            msg = msg + fields[2] + " closed room " + fields[3];
          }
          else
          {
            if (fields[1].equals("BOOT"))
            {
              send = false;
              





              this.bot.sendMessage(this.lobby, msg);
              return;
            }
            if (fields[1].equals("INF"))
            {
              msg = msg + fields[2] + " gave an infraction to " + fields[3] + " (" + fields[4] + ")";
              this.bot.sendMessage(this.lobby, msg);
              return;
            }
            if (fields[1].equals("RINF"))
            {
              msg = msg + fields[2] + " removed infraction " + fields[3];
              this.bot.sendMessage(this.lobby, msg);
              return;
            }
            if (fields[1].equals("RAW")) {
              msg = msg + fields[2];
            }
          }
        }
      }
      else if (fields[0].equals("E"))
      {
        String text = "";
        for (int i = 1; i < fields.length; i++) {
          text = text + fields[i] + "/";
        }
        text = text.substring(0, text.length() - 1);
        msg = msg + "\00304Error: " + text;
      }
      else
      {
        if (fields[0].equals("H"))
        {
          msg = msg + "\00304Hack: ";
          if (fields[1].equals("CHEAT"))
          {
            msg = msg + fields[2] + " was detected cheating using " + fields[3];
          }
          else
          {
            String text = "";
            for (int i = 1; i < fields.length; i++) {
              text = text + fields[i] + "/";
            }
            text = text.substring(0, text.length() - 1);
            msg = msg + text;
          }
          this.bot.sendMessage(this.lobby, msg);
          return;
        }
        if (fields[0].equals("R"))
        {
          msg = msg + "\00311Response: ";
          String text = "";
          for (int i = 1; i < fields.length; i++) {
            text = text + fields[i] + "/";
          }
          text = text.substring(0, text.length() - 1);
          msg = msg + text;
        }
      }
    }
    if ((!msg.equals("[" + timestamp + "] ")) && (send)) {
      this.bot.sendMessage(this.logs, msg);
    }
  }
  
  public void log(String msg)
  {
    this.bot.sendMessage("Azami", msg);
  }
  
  public void log(boolean msg)
  {
    if (msg) {
      this.bot.sendMessage("Azami", "true");
    } else {
      this.bot.sendMessage("Azami", "false");
    }
  }
  
  public void log(int msg)
  {
    String newMsg = new Integer(msg).toString();
    this.bot.sendMessage("Azami", newMsg);
  }
}
