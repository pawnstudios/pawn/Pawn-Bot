package PawnBot;

import PawnBot.pircbot.*;
import java.util.*;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.io.*;
import java.net.InetSocketAddress;
import it.gotoandplay.smartfoxserver.data.Zone;
import it.gotoandplay.smartfoxserver.data.Room;
import it.gotoandplay.smartfoxserver.extensions.*;
import it.gotoandplay.smartfoxserver.lib.*;
import it.gotoandplay.smartfoxserver.util.Attachment;
import it.gotoandplay.smartfoxserver.db.*;

public final class PawnBot extends PircBot {
	
	public PawnBotMain parent;
	public static PawnBot instance;
	
	public PawnBot (String name, PawnBotMain parent) {
		this.setName(name);
		this.parent = parent;
		
		instance = this;
	}
	
	public void onNotice (String sourceNick, String sourceLogin, String sourceHostname, String target, String notice) {
		notice = Colors.removeFormattingAndColors(notice);
		if (sourceNick.equals(parent.nickserv) && target.equals(parent.nick) && notice.indexOf(parent.nsIdent) != -1) {
			parent.identify();
			parent.checkChannels();
		}
	}
	
	public void onDisconnect () {
		if (!parent.shutdown) {
			parent.login();
		}
	}
	
	public void onMessage (String channel, String sender, String login, String hostname, String message) {
		User [] userList = this.getUsers(channel);
		User user = null;
		for (int i = 0; i < userList.length; i++) {
			if (userList[i].getNick().equals(sender)) {
				user = userList[i];
				break;
			}
		}
		if (user != null) {
			String [] fields = message.split(" ");
			if (fields.length > 0) {
				if (fields[0].equalsIgnoreCase("!listen")) {
					Zone zone = parent._server.getZone(parent.getOwnerZone());
					if (fields.length == 1) {
						this.sendMessage(channel, Colors.CYAN + "Active Channel List");
						String roomName;
						for (Iterator i = parent.listenList.keySet().iterator(); i.hasNext(); ) {
							roomName = i.next().toString();
							this.sendMessage(channel, Colors.CYAN + roomName + " (" + zone.getRoomByName(roomName).getId() + ")");
						}
						this.sendMessage(channel, Colors.CYAN + "End of List");
					} else {
						Room room = zone.getRoom(Integer.valueOf(fields[1]).intValue());
						if (room.getId() == Integer.valueOf(fields[1]).intValue()) {
							parent.listenList.put(room.getName(), new Integer(0));
							this.sendMessage(channel, Colors.CYAN + "Started listening on room " + room.getName());
						} else {
							this.sendMessage(channel, Colors.CYAN + "Could not find room with an id of " + fields[1]);
						}
					}
				} else if (fields[0].equalsIgnoreCase("!silence")) {
					if (fields.length == 1) {
						for (Iterator i = parent.listenList.keySet().iterator(); i.hasNext(); ) {
							String key = (String) (i.next());
							Integer value = (Integer) (parent.listenList.get(key));
							if (value.equals(new Integer(0))) {
								parent.listenList.remove(key);
							}
						}
						this.sendMessage(channel, Colors.CYAN + "Rooms silenced");
					} else {
						Zone zone = parent._server.getZone(parent.getOwnerZone());
						String roomName = zone.getRoom(Integer.valueOf(fields[1]).intValue()).getName();
						if (parent.listenList.containsKey(roomName)) {
							if (parent.listenList.get(roomName).equals(new Integer(0))) {
								parent.listenList.remove(roomName);
								this.sendMessage(channel, Colors.CYAN + "Room silenced");
							} else {
								this.sendMessage(channel, Colors.CYAN + "The specified room is not silencable");
							}
						} else {
							this.sendMessage(channel, Colors.CYAN + "The specified room is not active");
						}
					}
				} else if (fields[0].equalsIgnoreCase("!roomlist")) {
					Zone zone = parent._server.getZone(parent.getOwnerZone());
					LinkedList roomList = zone.getRoomList();
					Room room;
					String type;
					String msg = Colors.CYAN;
					for (Iterator i = roomList.iterator(); i.hasNext(); ) {
						room = (Room) (i.next());
						if (room.isGame()) {
							type = "Game";
						} else {
							type = "Chat";
						}
						if (fields.length > 1) {
							if ((((room.isGame() && fields[1].indexOf("g") != -1) || (!room.isGame() && fields[1].indexOf("c") != -1)) && ((room.isPrivate() && fields[1].indexOf("p") != -1) || (!room.isPrivate() && fields[1].indexOf("o") != -1))) || (fields[1].indexOf("g") == -1 && fields[1].indexOf("c") == -1 && fields[1].indexOf("p") == -1 && fields[1].indexOf("o") == -1)) {
								if (fields[1].indexOf("s") != -1) {
									msg += room.getName() + " (" + room.getId() + ") --- ";
								} else {
									msg += type + ": " + room.getName() + " (" + room.getId() + ") [" + room.howManyUsers() + "/" + room.getMaxUsers() + " users] --- ";
								}
								if (msg.length() > 300) {
									msg = msg.substring (0, msg.length() - 5);
									this.sendMessage(channel, msg);
									msg = Colors.CYAN;
								}
							}
						} else {
							msg += type + ": " + room.getName() + " (" + room.getId() + ") [" + room.howManyUsers() + "/" + room.getMaxUsers() + " users] --- ";
							if (msg.length() > 300) {
								msg = msg.substring (0, msg.length() - 5);
								this.sendMessage(channel, msg);
								msg = Colors.CYAN;
							}
						}
					}
					if (!msg.equals(Colors.CYAN)) {
						msg = msg.substring (0, msg.length() - 5);
						this.sendMessage(channel, msg);
					}
				} else if (fields[0].equalsIgnoreCase("!roomid")) {
					if (fields.length > 1) {
						Zone zone = parent._server.getZone(parent.getOwnerZone());
						this.sendMessage(channel, Colors.CYAN + "Room id for " + fields[1] + ": " + zone.getRoomByName(fields[1]).getId());
					} else {
						this.sendMessage(channel, Colors.CYAN + "Invalid Syntax");
					}
				} else if (fields[0].equalsIgnoreCase("!roominfo")) {
					if (fields.length > 1) {
						Zone zone = parent._server.getZone(parent.getOwnerZone());
						Room room = zone.getRoom(Integer.parseInt(fields[1]));
						if (room != null) {
							it.gotoandplay.smartfoxserver.data.User host = parent._server.getUserByChannel(room.getCreator());
							String types = "";
							if (room.isGame()) {
								types += "Game, ";
							} else {
								types += "Chat, ";
							}
							if (room.isPrivate()) {
								types += "Passworded";
							} else {
								types += "Open";
							}
							String msg = Colors.CYAN;
							it.gotoandplay.smartfoxserver.data.User [] userlist = room.getAllUsers();
							msg += "Room info for room " + room.getName() + " (" + room.getId() + ")";
							if (room.isPrivate()) {
								msg += " --- Password: " + room.getPassword();
							}
							msg +=  " --- Types: " + types + " --- Player Count: " + room.howManyUsers() + "/" + room.getMaxUsers();
							if (host != null) {
								msg +=  " --- Creator: " + host.getName() + " (" + host.getIpAddress() + ")";
							}
							this.sendMessage(channel, msg);
							this.sendMessage(channel, Colors.CYAN + "Player List:");
							String playerList = Colors.CYAN;
							for (int i = 0; i < userlist.length; i++) {
								if (userlist[i] != null) {
									playerList += userlist[i].getName() + " (" + userlist[i].getIpAddress() + ") ";
								} else {
									playerList += "null (null) ";
								}
								if (playerList.length() > 300) {
									playerList = playerList.substring (0, playerList.length() - 1);
									this.sendMessage(channel, playerList);
									playerList = Colors.CYAN;
								}
							}
							if (!playerList.equals(Colors.CYAN)) {
								this.sendMessage(channel, playerList.substring(0, playerList.length() - 1));
							}
						} else {
							this.sendMessage(channel, Colors.CYAN + "Room does not exist");
						}
					} else {
						this.sendMessage(channel, Colors.CYAN + "Invalid Syntax");
					}
				} else if (fields[0].equalsIgnoreCase("!info")) {
					Zone zone = parent._server.getZone(parent.getOwnerZone());
					Zone regZone = parent._server.getZone("PAWNreg");
					//this.sendMessage(channel, Colors.CYAN + "-- Pawn Server Information --");
					this.sendMessage(channel, Colors.CYAN + "Users connected: " + zone.getUserCount() + " online + " + regZone.getUserCount() + " registering (" + (zone.getUserCount() + regZone.getUserCount()) + " total) -- Total Rooms: " + zone.getRoomCount() + " -- Active Threads: " + Thread.activeCount());
				} else if (fields[0].equalsIgnoreCase("!msg")) {
					if (fields.length > 2) {
						Zone zone = parent._server.getZone(parent.getOwnerZone());
						Room room = zone.getRoom(Integer.parseInt(fields[1]));
						ActionscriptObject ao = new ActionscriptObject();
						String msg = "";

						for (int i = 2; i < fields.length; i++) {
							msg += fields[i] + " ";
						}
						msg = msg.substring(0, msg.length() - 1);

						ao.put("cmd", "ircMsg");
						ao.put("name", sender);
						ao.put("msg", msg);
						if (user.isOp()) {
							ao.put("flag", "2");
						} else {
							ao.put("flag", "1");
						}

						parent.sendResponse(ao, -1, null, room.getChannellList());
						parent.parseLogs("C/" + room.getName() + "/" + sender + "/" + msg);
					} else {
						this.sendMessage(channel, Colors.CYAN + "Invalid syntax. Type '!help msg' for more information.");
					}
				} else if (fields[0].equalsIgnoreCase("!help")) {
					if (fields.length == 1) {
						this.sendMessage(channel, Colors.CYAN + "PawnBot - Help");
						this.sendMessage(channel, Colors.CYAN + "To use a command use the syntax: '!CMD PARAM1 PARAM2 ...'");
						this.sendMessage(channel, Colors.CYAN + "Where CMD is any of the following commands:");
						this.sendMessage(channel, Colors.CYAN + "Mod Commands - listen, silence, roomlist, roomid, roominfo, msg, info, exec");
						this.sendMessage(channel, Colors.CYAN + "Admin Commands - ");
						this.sendMessage(channel, Colors.CYAN + "Type '!help CMD' for an explaination of each command");
					} else {
						if (fields[1].equalsIgnoreCase("listen")) {
							this.sendMessage(channel, Colors.CYAN + "Usage: !listen {Room ID(Optional)}");
							this.sendMessage(channel, Colors.CYAN + "Makes the bot start outputting text from the specified room. If no room id is specified then the active rooms are listed.");
						} else if (fields[1].equalsIgnoreCase("silence")) {
							this.sendMessage(channel, Colors.CYAN + "Usage: !silence {Room ID(Optional)}");
							this.sendMessage(channel, Colors.CYAN + "Makes the bot stop outputting text from the specified room. If no room id is specified then all active rooms are silenced.");
						} else if (fields[1].equalsIgnoreCase("roomlist")) {
							this.sendMessage(channel, Colors.CYAN + "Usage: !roomlist {Modifier parameters}");
							this.sendMessage(channel, Colors.CYAN + "Lists all of the rooms in the server if no modifiers are specified. Modifier parameters include: g (Game Rooms), c (Chat Rooms), p (Passworded Rooms), o (Open Rooms), s (Short description).");
						} else if (fields[1].equalsIgnoreCase("roomid")) {
							this.sendMessage(channel, Colors.CYAN + "Usage: !roomid {Room Name}");
							this.sendMessage(channel, Colors.CYAN + "Returns the room id for the specified room name. Room names are case-sensitive.");
						} else if (fields[1].equalsIgnoreCase("roomlist")) {
							this.sendMessage(channel, Colors.CYAN + "Usage: !roominfo {Room ID}");
							this.sendMessage(channel, Colors.CYAN + "Returns information about the room.");
						} else if (fields[1].equalsIgnoreCase("msg")) {
							this.sendMessage(channel, Colors.CYAN + "Usage: !msg {Room ID} {Message}");
							this.sendMessage(channel, Colors.CYAN + "Messages the specified room.");
						} else if (fields[1].equalsIgnoreCase("info")) {
							this.sendMessage(channel, Colors.CYAN + "Usage: !info");
							this.sendMessage(channel, Colors.CYAN + "Displays general server information.");
						} else if (fields[1].equalsIgnoreCase("exec")) {
							this.sendMessage(channel, Colors.CYAN + "Usage: !exec {Command} {Params-1} {Param-2} {Param-3} ... {Param-N}");
							this.sendMessage(channel, Colors.CYAN + "Executes a Pawn command. To get a list of available pawn commands type '!exec help'.");
						} else {
							this.sendMessage(channel, Colors.CYAN + "Command not found.");
						}
					}
				} else if (fields[0].equalsIgnoreCase("!exec")) {
					if (fields[1].equalsIgnoreCase("welcome") || fields[1].equalsIgnoreCase("createroom") || fields[1].equalsIgnoreCase("reply") || fields[1].equalsIgnoreCase("r")) {
						parent.parseLogs("R/This command cannot be used from irc");
					} else {
						ISmartFoxExtension ext = parent._server.getZone(parent.getOwnerZone()).getExtManager().get("pawnLobby");
						if (ext != null) {
							try {
								Zone zone = parent._server.getZone(parent.getOwnerZone());
								ActionscriptObject ao = new ActionscriptObject();
								ao.put("msg", message.substring(6, message.length()));
								SocketChannel chan = SocketChannel.open();
								chan.connect(new InetSocketAddress("127.0.0.1", 9339));
								chan.configureBlocking(false);
								chan.register(it.gotoandplay.smartfoxserver.SmartFoxServer.getInstance().getReadSelector(), SelectionKey.OP_READ, new Attachment());
								it.gotoandplay.smartfoxserver.data.User guser = new it.gotoandplay.smartfoxserver.data.User (chan, sender, zone.getName());
								if (user.isOp()) {
									guser.setVariable("flag", "2", "n");
								} else {
									guser.setVariable("flag", "1", "n");
								}
								guser.setVariable("lastPM", "", "s");
								ext.handleRequest("command", ao, guser, -1);
							} catch (IOException io) {
								parent.parseLogs("R/Command failed to execute!");
							}
						}
					}
				}
			}
		}
	}
	
	public void onInvite (String targetNick, String sourceNick, String sourceLogin, String sourceHostname, String channel) {
		if (channel.equalsIgnoreCase(parent.lobby) || channel.equalsIgnoreCase(parent.logs)) {
			this.sendMessage(parent.nickserv, "identify " + parent.pass);
			this.joinChannel(channel);
		}
	}
}
