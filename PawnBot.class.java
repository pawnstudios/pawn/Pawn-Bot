package PawnBot;

import PawnBot.pircbot.Colors;
import PawnBot.pircbot.PircBot;
import it.gotoandplay.smartfoxserver.SmartFoxServer;
import it.gotoandplay.smartfoxserver.data.Room;
import it.gotoandplay.smartfoxserver.data.Zone;
import it.gotoandplay.smartfoxserver.extensions.ExtensionHelper;
import it.gotoandplay.smartfoxserver.extensions.ExtensionManager;
import it.gotoandplay.smartfoxserver.extensions.ISmartFoxExtension;
import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;
import it.gotoandplay.smartfoxserver.util.Attachment;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

public final class PawnBot
  extends PircBot
{
  public PawnBotMain parent;
  public static PawnBot instance;
  
  public PawnBot(String name, PawnBotMain parent)
  {
    setName(name);
    this.parent = parent;
    
    instance = this;
  }
  
  public void onNotice(String sourceNick, String sourceLogin, String sourceHostname, String target, String notice)
  {
    notice = Colors.removeFormattingAndColors(notice);
    if ((sourceNick.equals(this.parent.nickserv)) && (target.equals(this.parent.nick)) && (notice.indexOf(this.parent.nsIdent) != -1))
    {
      this.parent.identify();
      this.parent.checkChannels();
    }
  }
  
  public void onDisconnect()
  {
    if (!this.parent.shutdown) {
      this.parent.login();
    }
  }
  
  public void onMessage(String channel, String sender, String login, String hostname, String message)
  {
    PawnBot.pircbot.User[] userList = getUsers(channel);
    PawnBot.pircbot.User user = null;
    for (int i = 0; i < userList.length; i++) {
      if (userList[i].getNick().equals(sender))
      {
        user = userList[i];
        break;
      }
    }
    if ((user.getPrefix().equals("+")) && (message.substring(0, 1).equals("!")))
    {
      sendNotice(sender, "Error: You do not have the correct permissions to use this bot.");
      return;
    }
    if (user != null)
    {
      String[] fields = message.split(" ");
      if (fields.length > 0) {
        if (fields[0].equalsIgnoreCase("!listen"))
        {
          Zone zone = this.parent._server.getZone(this.parent.getOwnerZone());
          if (fields.length == 1)
          {
            sendMessage(channel, "\00311Active Channel List");
            for (Iterator i = this.parent.listenList.keySet().iterator(); i.hasNext();)
            {
              String roomName = i.next().toString();
              sendMessage(channel, "\00311" + roomName + " (" + zone.getRoomByName(roomName).getId() + ")");
            }
            sendMessage(channel, "\00311End of List");
          }
          else
          {
            Room room = zone.getRoom(Integer.valueOf(fields[1]).intValue());
            if (room.getId() == Integer.valueOf(fields[1]).intValue())
            {
              this.parent.listenList.put(room.getName(), new Integer(0));
              sendMessage(channel, "\00311Started listening on room " + room.getName());
            }
            else
            {
              sendMessage(channel, "\00311Could not find room with an id of " + fields[1]);
            }
          }
        }
        else if (fields[0].equalsIgnoreCase("!silence"))
        {
          if (fields.length == 1)
          {
            for (Iterator i = this.parent.listenList.keySet().iterator(); i.hasNext();)
            {
              String key = (String)i.next();
              Integer value = (Integer)this.parent.listenList.get(key);
              if (value.equals(new Integer(0))) {
                this.parent.listenList.remove(key);
              }
            }
            sendMessage(channel, "\00311Rooms silenced");
          }
          else
          {
            Zone zone = this.parent._server.getZone(this.parent.getOwnerZone());
            String roomName = zone.getRoom(Integer.valueOf(fields[1]).intValue()).getName();
            if (this.parent.listenList.containsKey(roomName))
            {
              if (this.parent.listenList.get(roomName).equals(new Integer(0)))
              {
                this.parent.listenList.remove(roomName);
                sendMessage(channel, "\00311Room silenced");
              }
              else
              {
                sendMessage(channel, "\00311The specified room is not silencable");
              }
            }
            else {
              sendMessage(channel, "\00311The specified room is not active");
            }
          }
        }
        else if (fields[0].equalsIgnoreCase("!roomlist"))
        {
          Zone zone = this.parent._server.getZone(this.parent.getOwnerZone());
          LinkedList roomList = zone.getRoomList();
          

          String msg = "\00311";
          for (Iterator i = roomList.iterator(); i.hasNext();)
          {
            Room room = (Room)i.next();
            String type;
            String type;
            if (room.isGame()) {
              type = "Game";
            } else {
              type = "Chat";
            }
            if (fields.length > 1)
            {
              if (((room.isGame()) && (fields[1].indexOf("g") != -1)) || (((!room.isGame()) && (fields[1].indexOf("c") != -1) && (((room.isPrivate()) && (fields[1].indexOf("p") != -1)) || ((!room.isPrivate()) && (fields[1].indexOf("o") != -1)))) || ((fields[1].indexOf("g") == -1) && (fields[1].indexOf("c") == -1) && (fields[1].indexOf("p") == -1) && (fields[1].indexOf("o") == -1))))
              {
                if (fields[1].indexOf("s") != -1) {
                  msg = msg + room.getName() + " (" + room.getId() + ") --- ";
                } else {
                  msg = msg + type + ": " + room.getName() + " (" + room.getId() + ") [" + room.howManyUsers() + "/" + room.getMaxUsers() + " users] --- ";
                }
                if (msg.length() > 300)
                {
                  msg = msg.substring(0, msg.length() - 5);
                  sendMessage(channel, msg);
                  msg = "\00311";
                }
              }
            }
            else
            {
              msg = msg + type + ": " + room.getName() + " (" + room.getId() + ") [" + room.howManyUsers() + "/" + room.getMaxUsers() + " users] --- ";
              if (msg.length() > 300)
              {
                msg = msg.substring(0, msg.length() - 5);
                sendMessage(channel, msg);
                msg = "\00311";
              }
            }
          }
          if (!msg.equals("\00311"))
          {
            msg = msg.substring(0, msg.length() - 5);
            sendMessage(channel, msg);
          }
        }
        else if (fields[0].equalsIgnoreCase("!roomid"))
        {
          if (fields.length > 1)
          {
            Zone zone = this.parent._server.getZone(this.parent.getOwnerZone());
            sendMessage(channel, "\00311Room id for " + fields[1] + ": " + zone.getRoomByName(fields[1]).getId());
          }
          else
          {
            sendMessage(channel, "\00311Invalid Syntax");
          }
        }
        else if (fields[0].equalsIgnoreCase("!roominfo"))
        {
          if (fields.length > 1)
          {
            Zone zone = this.parent._server.getZone(this.parent.getOwnerZone());
            Room room = zone.getRoom(Integer.parseInt(fields[1]));
            if (room != null)
            {
              it.gotoandplay.smartfoxserver.data.User host = this.parent._server.getUserByChannel(room.getCreator());
              String types = "";
              if (room.isGame()) {
                types = types + "Game, ";
              } else {
                types = types + "Chat, ";
              }
              if (room.isPrivate()) {
                types = types + "Passworded";
              } else {
                types = types + "Open";
              }
              String msg = "\00311";
              it.gotoandplay.smartfoxserver.data.User[] userlist = room.getAllUsers();
              msg = msg + "Room info for room " + room.getName() + " (" + room.getId() + ")";
              if (room.isPrivate()) {
                msg = msg + " --- Password: " + room.getPassword();
              }
              msg = msg + " --- Types: " + types + " --- Player Count: " + room.howManyUsers() + "/" + room.getMaxUsers();
              if (host != null) {
                msg = msg + " --- Creator: " + host.getName() + " (" + host.getIpAddress() + ")";
              }
              sendMessage(channel, msg);
              sendMessage(channel, "\00311Player List:");
              String playerList = "\00311";
              for (int i = 0; i < userlist.length; i++)
              {
                if (userlist[i] != null) {
                  playerList = playerList + userlist[i].getName() + " (" + userlist[i].getIpAddress() + ") ";
                } else {
                  playerList = playerList + "null (null) ";
                }
                if (playerList.length() > 300)
                {
                  playerList = playerList.substring(0, playerList.length() - 1);
                  sendMessage(channel, playerList);
                  playerList = "\00311";
                }
              }
              if (!playerList.equals("\00311")) {
                sendMessage(channel, playerList.substring(0, playerList.length() - 1));
              }
            }
            else
            {
              sendMessage(channel, "\00311Room does not exist");
            }
          }
          else
          {
            sendMessage(channel, "\00311Invalid Syntax");
          }
        }
        else if (fields[0].equalsIgnoreCase("!info"))
        {
          Zone zone = this.parent._server.getZone(this.parent.getOwnerZone());
          Zone regZone = this.parent._server.getZone("PAWNreg");
          
          sendMessage(channel, "\00311Users connected: " + zone.getUserCount() + " online + " + regZone.getUserCount() + " registering (" + (zone.getUserCount() + regZone.getUserCount()) + " total) -- Total Rooms: " + zone.getRoomCount() + " -- Active Threads: " + Thread.activeCount());
        }
        else if (fields[0].equalsIgnoreCase("!msg"))
        {
          if (fields.length > 2)
          {
            Zone zone = this.parent._server.getZone(this.parent.getOwnerZone());
            Room room = zone.getRoom(Integer.parseInt(fields[1]));
            ActionscriptObject ao = new ActionscriptObject();
            String msg = "";
            for (int i = 2; i < fields.length; i++) {
              msg = msg + fields[i] + " ";
            }
            msg = msg.substring(0, msg.length() - 1);
            
            ao.put("cmd", "ircMsg");
            ao.put("name", sender);
            ao.put("msg", msg);
            if (user.isOp()) {
              ao.put("flag", "2");
            } else {
              ao.put("flag", "1");
            }
            this.parent.sendResponse(ao, -1, null, room.getChannellList());
            this.parent.parseLogs("C/" + room.getName() + "/" + sender + "/" + msg);
          }
          else
          {
            sendMessage(channel, "\00311Invalid syntax. Type '!help msg' for more information.");
          }
        }
        else if (fields[0].equalsIgnoreCase("!help"))
        {
          if (fields.length == 1)
          {
            sendMessage(channel, "\00311PawnBot - Help");
            sendMessage(channel, "\00311To use a command use the syntax: '!CMD PARAM1 PARAM2 ...'");
            sendMessage(channel, "\00311Where CMD is any of the following commands:");
            sendMessage(channel, "\00311Mod Commands - listen, silence, roomlist, roomid, roominfo, msg, info, exec");
            sendMessage(channel, "\00311Admin Commands - ");
            sendMessage(channel, "\00311Type '!help CMD' for an explaination of each command");
          }
          else if (fields[1].equalsIgnoreCase("listen"))
          {
            sendMessage(channel, "\00311Usage: !listen {Room ID(Optional)}");
            sendMessage(channel, "\00311Makes the bot start outputting text from the specified room. If no room id is specified then the active rooms are listed.");
          }
          else if (fields[1].equalsIgnoreCase("silence"))
          {
            sendMessage(channel, "\00311Usage: !silence {Room ID(Optional)}");
            sendMessage(channel, "\00311Makes the bot stop outputting text from the specified room. If no room id is specified then all active rooms are silenced.");
          }
          else if (fields[1].equalsIgnoreCase("roomlist"))
          {
            sendMessage(channel, "\00311Usage: !roomlist {Modifier parameters}");
            sendMessage(channel, "\00311Lists all of the rooms in the server if no modifiers are specified. Modifier parameters include: g (Game Rooms), c (Chat Rooms), p (Passworded Rooms), o (Open Rooms), s (Short description).");
          }
          else if (fields[1].equalsIgnoreCase("roomid"))
          {
            sendMessage(channel, "\00311Usage: !roomid {Room Name}");
            sendMessage(channel, "\00311Returns the room id for the specified room name. Room names are case-sensitive.");
          }
          else if (fields[1].equalsIgnoreCase("roomlist"))
          {
            sendMessage(channel, "\00311Usage: !roominfo {Room ID}");
            sendMessage(channel, "\00311Returns information about the room.");
          }
          else if (fields[1].equalsIgnoreCase("msg"))
          {
            sendMessage(channel, "\00311Usage: !msg {Room ID} {Message}");
            sendMessage(channel, "\00311Messages the specified room.");
          }
          else if (fields[1].equalsIgnoreCase("info"))
          {
            sendMessage(channel, "\00311Usage: !info");
            sendMessage(channel, "\00311Displays general server information.");
          }
          else if (fields[1].equalsIgnoreCase("exec"))
          {
            sendMessage(channel, "\00311Usage: !exec {Command} {Params-1} {Param-2} {Param-3} ... {Param-N}");
            sendMessage(channel, "\00311Executes a Pawn command. To get a list of available pawn commands type '!exec help'.");
          }
          else
          {
            sendMessage(channel, "\00311Command not found.");
          }
        }
        else if (fields[0].equalsIgnoreCase("!exec"))
        {
          if ((fields[1].equalsIgnoreCase("welcome")) || (fields[1].equalsIgnoreCase("createroom")) || (fields[1].equalsIgnoreCase("reply")) || (fields[1].equalsIgnoreCase("r")))
          {
            this.parent.parseLogs("R/This command cannot be used from irc");
          }
          else
          {
            ISmartFoxExtension ext = this.parent._server.getZone(this.parent.getOwnerZone()).getExtManager().get("pawnLobby");
            if (ext != null) {
              try
              {
                Zone zone = this.parent._server.getZone(this.parent.getOwnerZone());
                ActionscriptObject ao = new ActionscriptObject();
                ao.put("msg", message.substring(6, message.length()));
                SocketChannel chan = SocketChannel.open();
                chan.connect(new InetSocketAddress("127.0.0.1", 9339));
                chan.configureBlocking(false);
                chan.register(SmartFoxServer.getInstance().getReadSelector(), 1, new Attachment());
                it.gotoandplay.smartfoxserver.data.User guser = new it.gotoandplay.smartfoxserver.data.User(chan, sender, zone.getName());
                if (user.isOp()) {
                  guser.setVariable("flag", "2", "n");
                } else {
                  guser.setVariable("flag", "1", "n");
                }
                guser.setVariable("lastPM", "", "s");
                ext.handleRequest("command", ao, guser, -1);
              }
              catch (IOException io)
              {
                this.parent.parseLogs("R/Command failed to execute!");
              }
            }
          }
        }
      }
    }
  }
  
  public void onInvite(String targetNick, String sourceNick, String sourceLogin, String sourceHostname, String channel)
  {
    if ((channel.equalsIgnoreCase(this.parent.lobby)) || (channel.equalsIgnoreCase(this.parent.logs)))
    {
      sendMessage(this.parent.nickserv, "identify " + this.parent.pass);
      joinChannel(channel);
    }
  }
}
