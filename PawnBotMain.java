package PawnBot;

import java.io.*;
import java.util.HashMap;
import java.text.DateFormat;
import java.util.Date;

import PawnBot.pircbot.IrcException;
import PawnBot.pircbot.NickAlreadyInUseException;
import PawnBot.pircbot.Colors;

import it.gotoandplay.smartfoxserver.SmartFoxServer;
import it.gotoandplay.smartfoxserver.extensions.*;
import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;
import it.gotoandplay.smartfoxserver.events.InternalEventObject;

public class PawnBotMain extends AbstractExtension {
	
	//Config
	public String nick = "PawnBot";
	public String pass = "k5mnw35evn7y4ntwf";
	public String server = "irc.heuxe.org";
	public String lobby = "#staff";
	public String logs = "#Pawngateway";
	public String nsIdent = "This nickname is registered and protected.";
	public long messageDelay = 50;
	
	public String quitMsg = "PawnBot - Shutting down...";
	
	public String nickserv = "NickServ";
	public String chanserv = "ChanServ";
	public String operserv = "OperServ";
	
	//Vars
	public PawnBot bot;
	public boolean shutdown = false;
	public HashMap listenList = new HashMap();
	public ExtensionHelper _server = ExtensionHelper.instance();
	
	public void init() {
		bot = new PawnBot(nick, this);
		bot.setMessageDelay(messageDelay);
		
		listenList.put("Chat", new Integer(1));
		
		this.login ();
	}
	
	public void destroy() {
		shutdown = true;
		bot.quitServer(quitMsg);
		bot.dispose();
	}
	
	public void handleRequest(String cmd, ActionscriptObject ao, it.gotoandplay.smartfoxserver.data.User u, int fromRoom) {
		
	}
	
	public void handleRequest(String cmd, String params[], it.gotoandplay.smartfoxserver.data.User u, int fromRoom) {
		
	}
	
	public void handleInternalEvent(InternalEventObject ieo) {
		
	}
	
	//------------------------------------------------------
	// Methods
	//------------------------------------------------------
	public void login () {
		int error = this.connect();
		if (error == 0) {
			this.login();
		} else if (error == 1) {
			bot.sendMessage(nickserv, "ghost " + nick + " " + pass);
			bot.changeNick(nick);
		} else {
			
		}
	}
	
	public int connect () {
		try {
        	try {
        		try {
        			bot.connect(this.server);
        		} catch (NickAlreadyInUseException ni) {
        			return 1;
        		}
        	} catch (IrcException ir) {
        		return 0;
        	}
        } catch (IOException io) {
        	return 0;
        }
        return -1;
	}
	
	public void joinChannel (String chan) {
		bot.sendMessage(chanserv, "invite " + chan);
	}
	
	public void identify () {
		bot.sendMessage(nickserv, "identify " + pass);
		bot.sendMessage(chanserv, "OP ALL");
		//bot.sendRawLine("oper PawnBot k5mnw35evn7y4ntwf");
	}
	
	public void sendMessageToAllChannels (String msg) {
		bot.sendMessage(lobby, msg);
		bot.sendMessage(logs, msg);
	}
	
	public void checkChannels () {
		if (bot.getChannels().length != 2) {
			this.joinChannel(lobby);
			this.joinChannel(logs);
		}
	}
	
	public void parseLogs (String log) {
		String timestamp = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(new Date());
		String msg = "";
		String [] fields = log.split("/");
		boolean send = true;
		msg += "[" + timestamp + "] ";
		if (fields[0].equals("C")) {
			if (listenList.containsKey(fields[1])) {
				String text = "";
				for (int i = 3; i < fields.length; i++) {
					text += fields[i] + "/";
				}
				text = text.substring(0, text.length() - 1);
				msg += "(" + fields[1] + ") " + fields[2] + ": " + text;
			}
		} else if (fields[0].equals("P")) {
			String text = "";
			for (int i = 3; i < fields.length; i++) {
				text += fields[i] + "/";
			}
			text = text.substring(0, text.length() - 1);
			msg += Colors.DARK_GREEN + "(" + fields[1] + " > " + fields[2] + "): " + text;
		} else if (fields[0].equals("S")) {
			msg += Colors.YELLOW + "Server: ";
			if (fields.length == 2) {
				msg += fields[1];
			} else {
				if (fields[1].equals("LOGIN")) {
					//msg += "Player login - " + fields[2] + " (" + fields[3] + ")";
					send = false;
				} else if (fields[1].equals("ANNOUNCE")) {
					String text = "";
					for (int i = 3; i < fields.length; i++) {
						text += fields[i] + "/";
					}
					text = text.substring(0, text.length() - 1);
					msg += "Announcement from " + fields[2] + ": " + text;
				} else if (fields[1].equals("KICK")) {
					String text = "";
					for (int i = 4; i < fields.length; i++) {
						text += fields[i] + "/";
					}
					text = text.substring(0, text.length() - 1);
					msg += fields[2] + " kicked " + fields[3] + " (" + text + ")";
				} else if (fields[1].equals("BAN")) {
					String text = "";
					for (int i = 5; i < fields.length; i++) {
						text += fields[i] + "/";
					}
					text = text.substring(0, text.length() - 1);
					if (fields[2].equals("IP")) {
						msg += fields[3] + " banned the IP " + fields[4] + " (" + text + ")";
					} else if (fields[2].equals("ACCOUNT")) {
						msg += fields[3] + " banned the Account " + fields[4] + " (" + text + ")";
					} else if (fields[2].equals("USER")) {
						msg += fields[3] + " banned the User " + fields[4] + " (" + text + ")";
					} else if (fields[2].equals("KICKBAN")) {
						msg += fields[3] + " kick-banned the User " + fields[4] + " (" + text + ")";
					}
				} else if (fields[1].equals("UNBAN")) {
						msg += fields[2] + " unbanned " + fields[3];
				} else if (fields[1].equals("PROMOTE")) {
					String rank = "";
					if (fields[2].equals("ADMIN")) {
						rank = "Administrator";
					} else if (fields[2].equals("MOD")) {
						rank = "Moderator";
					}
					msg += fields[3] + " promoted " + fields[4] + " to " + rank;
				} else if (fields[1].equals("DEMOTE")) {
					String rank = "";
					if (fields[2].equals("USER")) {
						rank = "User";
					} else if (fields[2].equals("MOD")) {
						rank = "Moderator";
					}
					msg += fields[3] + " demoted " + fields[4] + " to " + rank;
				} else if (fields[1].equals("DELETE")) {
					msg += fields[2] + " deleted " + fields[3] + "'s account";
				} else if (fields[1].equals("CLOSEROOM")) {
					msg += fields[2] + " closed room " + fields[3];
				} else if (fields[1].equals("BOOT")) {
					send = false;
					/*String text = "";
					for (int i = 5; i < fields.length; i++) {
						text += fields[i] + "/";
					}
					text = text.substring(0, text.length() - 1);
					msg += "(" + fields[4] + ") " + fields[2] + " booted " + fields[3] + "  (" + text + ")";*/
				} else if (fields[1].equals("INF")) {
					msg += fields[2] + " gave an infraction to " + fields[3] + " (" + fields[4] + ")";
				} else if (fields[1].equals("RINF")) {
					msg += fields[2] + " removed infraction " + fields[3];
				} else if (fields[1].equals("RAW")) {
					msg += fields[2];
				}
			}
		} else if (fields[0].equals("E")) {
			String text = "";
			for (int i = 1; i < fields.length; i++) {
				text += fields[i] + "/";
			}
			text = text.substring(0, text.length() - 1);
			msg += Colors.RED + "Error: " + text;
		} else if (fields[0].equals("H")) {
			msg += Colors.RED + "Hack: ";
			if (fields[1].equals("CHEAT")) {
				msg += fields[2] + " was detected cheating using " + fields[3];
			} else {
				String text = "";
				for (int i = 1; i < fields.length; i++) {
					text += fields[i] + "/";
				}
				text = text.substring(0, text.length() - 1);
				msg += text;
			}
		} else if (fields[0].equals("R")) {
			msg += Colors.CYAN + "Response: ";
			String text = "";
			for (int i = 1; i < fields.length; i++) {
				text += fields[i] + "/";
			}
			text = text.substring(0, text.length() - 1);
			msg += text;
		}
		if (!msg.equals("[" + timestamp + "] ") && send) {
			bot.sendMessage(logs, msg);
		}
	}
	
	public void log (String msg) {
		bot.sendMessage("Azami", msg);
	}
	public void log (boolean msg) {
		if (msg) {
			bot.sendMessage("Azami", "true");
		} else {
			bot.sendMessage("Azami", "false");
		}
	}
	public void log (int msg) {
		String newMsg = new Integer(msg).toString();
		bot.sendMessage("Azami", newMsg);
	}
}