/**
Contains Bot Relevant Actionscript
**/

function writeToLogs(type, params) {
	//Write to logs
	var time = new java.util.Calendar.getInstance();
	var month = time.get(java.util.Calendar.MONTH) + 1;
	if (month < 10) {
		month = "0" + String(month);
	}
	var day = time.get(java.util.Calendar.DAY_OF_MONTH);
	if (day < 10) {
		day = "0" + String(day);
	}
	var year = time.get(java.util.Calendar.YEAR);
	
	var hour = time.get(java.util.Calendar.HOUR_OF_DAY);
	if (hour < 10) {
		hour = "0" + String(hour);
	}
	var minute = time.get(java.util.Calendar.MINUTE);
	if (minute < 10) {
		minute = "0" + String(minute);
	}
	var second = time.get(java.util.Calendar.SECOND);
	if (second < 10) {
		second = "0" + String(second);
	}
	
	if (!_server.isDir("Server Logs/" + year + "-" + month)) {
		_server.makeDir("Server Logs/" + year + "-" + month);
	}
	
	var targetLog = "Server Logs/" + year + "-" + month + "/" + day + ".txt";
	var timestamp = hour + ":" + minute + ":" + second;
	var msg = type;
	for (var i = 0; i < params.length; i++) {
		msg += "/" + params[i];
	}
	/*
	_server.writeFile (targetLog, timestamp + "/" + msg + "\n", true);
	*/
	
	//Echo to IRC Bot
	var extMan = _server.getCurrentZone().getExtManager();
	var botExt = extMan.get("bot");
	if (botExt != null) {
		botExt.parseLogs(msg); //Bot entry point from pawn server
	}
}
